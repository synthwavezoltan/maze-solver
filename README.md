# Maze generator & pathfinder (Lua/LÖVE)

A combined release of soemthing I'm proud of: one part of this program generates a random maze;
the other part finds the shortest path between two randomly selected points.

Written in Lua, with LÖVE framework used to display the result.
