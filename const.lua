--[[
----------------------------------------------------------------------------------------
									CONSTANTS
----------------------------------------------------------------------------------------
]]

-- colors
BLACK = {0, 0, 0}
GRAY1 = {0.1, 0.1, 0.1}
GRAY2 = {0.3, 0.3, 0.3}

BLUE = {0, 0, 0.8}
RED = {0.4, 0, 0}
GREEN = {0, 1, 0}

GRAY3 = {0.5, 0.5, 0.5}
GRAY4 = {0.4, 0.4, 0.4}
GRAY5 = {0.7, 0.7, 0.7}

ORANGE = {0.15, 0.05, 0}

-- maze generator meaning codes
CANDIDATE = 2
FORBIDDEN = 10

-- backtracking meaning codes
WALL = 1
PATH = 4
DEAD_END = 5
GOAL = 6
FREE = 9

-- misc settings
GENERATE_SPEED = 2^5

SPEED = 2^2

-- color table
colors = {
    BLACK, --WALL
    GRAY1, --CANDIDATE
    GRAY2,
    
	BLUE, --PATH
	RED, --DEAD_END
	GREEN, --GOAL
    
	GRAY3,
	GRAY4,
    GRAY5, --GOAL
    
    ORANGE, --FORBIDDEN
}