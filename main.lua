require 'const'
require 'utils'
require 'maze_generator'

math.randomseed( os.time() )

-- "GENERATE", "BACKTRACK", "DONE"
MODE = "GENERATE"
MAZE_SIZE = 0

tileSize = 8
TileX, TileY = 0,0
SizeX, SizeY = love.graphics.getWidth() / tileSize / 1, love.graphics.getHeight() / tileSize / 1
counter = 0

cl = 0

StartX, StartY = 1,1
FinishX,FinishY = 1,1
SelectedX, SelectedY = 1,1

function love.load()
	initiateMazeGenerator()
end

function love.draw()

	for j=1,SizeX do
		for i=1, SizeY do
			if Numbers[j][i] ~= 0 then
				love.graphics.setColor(colors[Numbers[j][i]])
				love.graphics.rectangle("fill", (j-1)*tileSize, (i-1)*tileSize, tileSize, tileSize)
				love.graphics.setColor(1,1,1)
			end			
		end
	end
	
	love.graphics.print(math.ceil(CountColors(5)/(MAZE_SIZE/100))..
						" % is explored\n"..MAZE_SIZE..
						"\n"..cl..
						"\n"..CountColors(2),0,0)

end

function love.update(dt)
	if MODE == "GENERATE" then
		if CountColors(CANDIDATE) ~= 0 then
			for i=0, GENERATE_SPEED do
				buildMaze()
			end

			MAZE_SIZE = CountColors(FREE)
		else
			--backtrack: start,finish,selector
			while Numbers[StartX][StartY] ~= FREE do 
				StartX, StartY = math.random(1,SizeX-1), math.random(1,SizeY-1)
			end

			Numbers[StartX][StartY] = PATH

			while Numbers[FinishX][FinishY] ~= FREE or CountNeighbours(FinishX, FinishY, PATH) ~= 0 do
				FinishX, FinishY = math.random(1,SizeX-1), math.random(1,SizeY-1)
			end

			Numbers[FinishX][FinishY] = GOAL	
			
			MAZE_SIZE = CountColors(FREE)
			SelectedX, SelectedY = StartX, StartY

			MODE = "BACKTRACK"
		end
	elseif MODE == "BACKTRACK" then
		for i=0, SPEED do
			if CountNeighbours(FinishX, FinishY, PATH) == 0 then
				backtrack(FREE, PATH)
				cl = cl+ 1
			end
		end
	elseif MODE == "DONE" then
		initiateMazeGenerator()
		MODE = "GENERATE"
	end
end

--[[
----------------------------------------------------------------------------------------
				THE BACKTRACKING PROGRAM TO FIND EXIT
				(made in April 2014 by Katamori, unoptimized yet)
----------------------------------------------------------------------------------------
]]

--1: you CAN visit (FREE=9)
--2: you CAN NOT visit (WALL=1)
--3: you ALREADY VISITED (PATH=4)
--4: WRONG WAY (DEAD_END=5)

function backtrack(condition, writeit)
	currentX, currentY = SelectedX, SelectedY

	-- victory condition
	if CountNeighbours(SelectedX, SelectedY, 6) ~= 0 then 
		MODE = "DONE"
		return
	end

	-- recolor pivot tile if condition is met; else do recursion
	if CountNeighbours(currentX, currentY, condition) ~= 0 then
		Numbers[currentX][currentY] = writeit
	else
		backtrack(writeit, DEAD_END)
	end

	-- if dead end, move away
	
	if SelectedY-1 > 0 and Numbers[SelectedX][SelectedY-1] == condition then
		SelectedY = SelectedY-1 --up
	elseif SelectedX+1 <= SizeX and Numbers[SelectedX+1][SelectedY] == condition then
		SelectedX = SelectedX+1 --right
	elseif SelectedY+1 <= SizeY and Numbers[SelectedX][SelectedY+1] == condition then
		SelectedY = SelectedY+1 --down
	elseif SelectedX-1 > 0 and Numbers[SelectedX-1][SelectedY] == condition then
		SelectedX = SelectedX-1 --left
	end
end


