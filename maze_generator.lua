require("utils")

--[[
----------------------------------------------------------------------------------------
				THE LABYRINTH GENERATOR AND NECESSARY FUNCTIONS
				(made in April 2014 by Katamori, unoptimized yet)
----------------------------------------------------------------------------------------
]]
function initiateMazeGenerator()
	Field = createField(1)
	Numbers = createField(0)
	HelpTable = createField(0)

	Numbers[math.random(1,SizeX)][math.random(1,SizeY)] = FREE
	buildMaze()
	buildMaze()
end

function buildMaze()
	copy(HelpTable, Numbers)	
	
	for j=1,SizeX-1 do
		for i=1, SizeY-1 do

			if Numbers[j][i] ~= FREE and Numbers[j][i] ~= FORBIDDEN then 
				if CountNeighbours(j,i, FREE) > 0 then
					HelpTable[j][i] = WALL
				end
			
				if CountNeighbours(j,i, FREE) == 1
				and FullNeighbours(j,i, FREE) < 3			
				and HelpTable[j][i] == WALL then
					HelpTable[j][i] = CANDIDATE 
				end
			end
		end
	end		

	candidates = ListColors(CANDIDATE)

	if #candidates ~= 0 then
        rand = candidates[math.random(#candidates)]

        HelpTable[rand[1]][rand[2]] = FREE
	end	
		
	copy(Numbers, HelpTable)
end

function CountNeighbours(x, y, val)
	return (x-1 > 0 and Numbers[x-1][y] == val and 1 or 0) +
	(x+1 <= SizeX and Numbers[x+1][y] == val and 1 or 0) +
	(y-1 > 0 and Numbers[x][y-1] == val and 1 or 0) +
	(y+1 <= SizeY and Numbers[x][y+1] == val and 1 or 0)
end

function FullNeighbours(x,y, valueToCheck)
	local asd=0

	for j=-1,1 do
		for i=-1, 1 do
			asd = asd + (x-1 > 0 and x+1 <= SizeX and y-1 > 0 and y+1 <= SizeY and Numbers[x-j][y-i] == valueToCheck and 1 or 0)
		end
	end
	
	return asd
end

function CountColors(valueToCheck)

	local asd=0
	for j=1,SizeX-1 do
		for i=1, SizeY-1 do
			if Numbers[j][i] == valueToCheck then asd=asd+1 end
		end
	end
	
	return asd
end

function ListColors(valueToCheck)

	local list = {}
	for j=1,SizeX-1 do
		for i=1, SizeY-1 do
			if HelpTable[j][i] == valueToCheck then table.insert(list, {j,i}) end
		end
	end
	
	return list
end



function copy(a,b)

	for j=1,SizeX do
		for i=1, SizeY do
			a[j][i] = b[j][i]
		end
	end

end